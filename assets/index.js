const taskInputElement = document.querySelector('.task_add-input');
const taskAddBtn = document.querySelector('.task_add-btn');
const taskListElement = document.querySelector('#task_list');

let listTask = [
  {
    id: '1',
    title: 'Đi chợ',
    done: false,
  },
  {
    id: '2',
    title: 'Đi nấu ăn',
    done: false,
  },
  {
    id: '3',
    title: 'Đi xem phim',
    done: true,
  },
];

let idEdit = '';

const render = () => {
  const htmlTask = listTask
    .map((task) => {
      return `
    <div class="task_list-item ${task.done ? 'task-done' : ''}">
      <span class="task_list-item_text" onclick="handelDoneTask('${task.id}')">${task.title}</span>
      <div class="task_list-item_action">
      <span class="task_list-item_edit" onclick="handelEditTask('${task.id}')">
        <i class="las la-pen-square"></i>
      </span>
      <span class="task_list-item_close" onclick="handelCloseTask('${task.id}')">
      <i class="las la-times"></i>
    </span>
    </div>
    </div>
  `;
    })
    .join('');
  taskListElement.innerHTML = htmlTask;
};
render();
const handelDoneTask = (id) => {
  const index = listTask.findIndex((task) => task.id === id);
  if (index >= 0) {
    listTask[index].done = !listTask[index].done;
  }
  render();
};

taskAddBtn.addEventListener('click', () => {
  if (!taskInputElement.value.length) {
    alert('Please enter the job name to do !');
    return;
  }
  if (!idEdit) {
    listTask.push({
      id: Math.random().toString(36).slice(2),
      title: taskInputElement.value,
      done: false,
    });
  } else {
    const index = listTask.findIndex((task) => task.id === idEdit);
    listTask[index].title = taskInputElement.value;
    taskAddBtn.textContent = 'Add';
    idEdit = '';
  }
  taskInputElement.value = '';
  render();
});

const handelCloseTask = (id) => {
  listTask = listTask.filter((task) => task.id !== id);
  render();
};

const handelEditTask = (id) => {
  const taskFind = listTask.find((task) => task.id === id);
  idEdit = taskFind.id;
  taskInputElement.value = taskFind.title;
  taskAddBtn.textContent = 'Edit';
};
